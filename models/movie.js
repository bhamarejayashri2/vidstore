const mongoose = require('mongoose');
const Joi = require('joi');
const {genreSchema} = require('./genre')

const movieSchema = new mongoose.Schema({
  title : String,
  genre : genreSchema,
  numberInStock : Number,
  dailyRentalRate : Number
})

const Movie = mongoose.model('Movie',movieSchema);

module.exports = {Movie};