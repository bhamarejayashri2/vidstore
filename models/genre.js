const mongoose = require('mongoose');
const Joi = require('joi');

const genreSchema = new mongoose.Schema({
  name : {
    type : String,
    required : true,
    minlength : 3,
    maxlength : 10
  }});

const Genre = mongoose.model('Genre',genreSchema);

function validateGenre(genre){
  const schema = Joi.object({
    name : Joi.string().required().min(3).max(10)
  });
  
  return schema.validate(genre);
}

module.exports = {genreSchema,Genre,validateGenre};