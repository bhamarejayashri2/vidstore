const mongoose = require('mongoose');
const Joi = require('joi');

const rentalSchema = new mongoose.Schema({
  Customer : {
    type : new mongoose.Schema({
      isGold : Boolean,
      name : String,
      phone : {
        type : String,
        minlength : 10,
        maxlength : 10
      }
    }),
    required : true
  },
  Movie : {
    type : new mongoose.Schema({
      title : String,
      movie_type : String,
      dailyRentalRate : Number
    }),
    required : true
  },
  dateOut : {
    type : Date,
    required : true,
    default : Date.now
  },
  dateReturned : Date,
  rentalFee : {
    type : Number,
    minlength : 0
  }
})

const Rental = mongoose.model('Rental',rentalSchema);

function validateRental(rental){
  const schema = Joi.object({
    customerId : Joi.string().required(),
    movieId : Joi.string().required()
  });
  
  return schema.validate(rental);
}

module.exports = {Rental,validateRental};