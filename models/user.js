const jwt = require('jsonwebtoken');
const config = require('config');
const mongoose = require('mongoose');
const Joi = require('joi');

const userSchema = new mongoose.Schema({
  name : {
    type : String,
    required : true,
    minlength : 5,
    maxlength : 30
  },
  email : {
    type : String,
    required : true,
    minlength : 10,
    maxlength : 255,
    unique : true
  },
  password : {
    type : String,
    required : true,
    minlength : 8,
    maxlength : 1024
  },
  isAdmin : Boolean
})

userSchema.methods.generateAuthToken = function(){
  const token = jwt.sign({_id : this._id,isAdmin : this.isAdmin},config.get('jwtPrivateKey'));
  return token;
}

const User = mongoose.model('User',userSchema);

function validateUser(user){
  const schema = Joi.object({
    name : Joi.string().required().min(5).max(30),
    email : Joi.string().required().min(10).max(255).email(),
    password : Joi.string().required().min(8).max(20)
  });
  
  return schema.validate(user);
}

module.exports = {User,validateUser};