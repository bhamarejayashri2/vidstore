const mongoose = require('mongoose');
const Joi = require('joi');

const customerSchema = new mongoose.Schema({
  isGold : {
    type : Boolean,
    default : true
  },
  name : {
    type : String,
    required : true,
    minlength : 3,
    maxlength : 20
  },
  phone : {
    type : Number,
    required : true
  }
});

const Customer = mongoose.model('Customer',customerSchema);

function validateCustomer(customer){
  const Schema = Joi.object({
    isGold : Joi.boolean(),
    name : Joi.string().required().min(3).max(20),
    phone : Joi.number().required()
  });

  return Schema.validate(customer);
}

// module.exports = Customer;
// module.exports.validate = validateCustomer;
module.exports = {
  Customer,
  validateCustomer
}