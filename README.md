# vidstore

## sample api request and response are mentioned below

### POST /api/movies
```
curl --location --request POST 'http://localhost:3000/api/movies' \
--header 'Content-Type: application/json' \
--data-raw '{
    "title" : "The Jungle Book",
    "genre" : "Thrillar",
    "numberInStock" : 2,
    "dailyRentalRate" :  100
}'

{
    "_id": "5fe20cb3742c623b800d35d5",
    "title": "The Jungle Book",
    "genre": {
        "_id": "5fe20cb3742c623b800d35d6",
        "name": "Thrillar"
    },
    "numberInStock": 2,
    "dailyRentalRate": 100,
    "__v": 0
}
```

### POST /api/customers
```
curl --location --request POST 'http://localhost:3000/api/customers' \
--header 'Content-Type: application/json' \
--data-raw '{
    "isGold" : "true",
    "name" : "Jim",
    "phone" : 6898968832
}'

{
    "isGold": true,
    "_id": "5fe20d98742c623b800d35d7",
    "name": "Jim",
    "phone": 6898968832,
    "__v": 0
}
```


### GET /api/customers
```
curl --location --request GET 'http://localhost:3000/api/customers' \
--data-raw ''

[
    {
        "isGold": true,
        "_id": "5fe20d98742c623b800d35d7",
        "name": "Jim",
        "phone": 6898968832,
        "__v": 0
    }
]
```

### GET /api/movies
```
curl --location --request GET 'http://localhost:3000/api/movies'

[
    {
        "_id": "5fe20cb3742c623b800d35d5",
        "title": "The Jungle Book",
        "genre": {
            "_id": "5fe20cb3742c623b800d35d6",
            "name": "Thrillar"
        },
        "numberInStock": 2,
        "dailyRentalRate": 100,
        "__v": 0
    }
]
```

### POST /api/rentals
```
curl --location --request POST 'http://localhost:3000/api/rentals' \
--header 'Content-Type: application/json' \
--data-raw '{
    "customerId" : "5fe20d98742c623b800d35d7",
    "movieId" : "5fe20cb3742c623b800d35d5"
}'

{
    "_id": "5fe20e84742c623b800d35da",
    "Customer": {
        "_id": "5fe20d98742c623b800d35d7",
        "name": "Jim",
        "phone": "6898968832"
    },
    "Movie": {
        "_id": "5fe20cb3742c623b800d35d5",
        "title": "The Jungle Book",
        "dailyRentalRate": 100
    },
    "dateOut": "2020-12-22T15:19:32.373Z",
    "__v": 0
}
```

### POST /api/users
```
curl --location --request POST 'http://localhost:3000/api/users' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name" : "Jayashri Bhamare",
    "email" : "bhamarejayashri2@gmail.com",
    "password" : "12345678"
}'

{
    "name": "Jayashri Bhamare",
    "email": "bhamarejayashri2@gmail.com"
}
```

### GET /api/users/me
```
curl --location --request GET 'http://localhost:3000/api/users/me' \
--header 'x-auth-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZmUyMGVjMTc0MmM2MjNiODAwZDM1ZGQiLCJpYXQiOjE2MDg2NTA0MzN9.UmrmjUcKX0RqV9Q0xof2DGzyIL2T26bbAhQl8zXiMMA' \
--data-raw ''

{
    "_id": "5fe20ec1742c623b800d35dd",
    "name": "Jayashri Bhamare",
    "email": "bhamarejayashri2@gmail.com",
    "__v": 0
}
```

### POST /api/genres
```
curl --location --request POST 'http://localhost:3000/api/genres' \
--header 'x-auth-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZmUyMGVjMTc0MmM2MjNiODAwZDM1ZGQiLCJpYXQiOjE2MDg2NTA0MzN9.UmrmjUcKX0RqV9Q0xof2DGzyIL2T26bbAhQl8zXiMMA' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name" : "Suspense"
}'

{
    "_id": "5fe20fd2742c623b800d35de",
    "name": "Suspense",
    "__v": 0
}
```
