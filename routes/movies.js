const { Movie } = require('../models/movie');
const express = require('express');
const router = express.Router();


router.get('/',async (req,res) => {
  const movie = await Movie.find();
  res.send(movie);
})

router.post('/',async (req,res) => {
  //const { title, genre } = req.body // {title: 'something', genre:'something'}

  let movie = new Movie({
    title : req.body.title,
    genre : {name : req.body.genre},
    numberInStock : req.body.numberInStock,
    dailyRentalRate : req.body.dailyRentalRate
  });

  movie = await movie.save();
  res.send(movie);
})

module.exports = router;