const {Customer, validateCustomer: validate } = require('../models/customer');
//const Joi = require('joi');
const express = require('express');
const router = express.Router();
//const mongoose = require('mongoose');

router.get('/:id',async (req,res) => {
  const customer = await Customer.findById(req.params.id);

  if(!customer) return res.status(404).send('The customer with given id was not found');

  res.send(customer);
})

router.get('/',async (req,res) => {
  const customer = await Customer.find().sort();
  res.send(customer);
})

router.post('/',async (req,res) => {
  try{
    const {error} = validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    let customer = new Customer(
        { isGold : req.body.isGold,
          name : req.body.name,
          phone : req.body.phone
        });

    customer = await customer.save();
    res.send(customer);
  }
  catch(ex){
    res.status(400).send(ex.message);
  }  
})

router.put('/:id',async (req,res) => {
  try{
    const { error } = validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    const customer = await Customer.findByIdAndUpdate(req.params.id,
      {isGold : req.body.isGold,
        name : req.body.name,
        phone : req.body.phone},
        {new : true});
    
    if(!customer) return res.status(404).send('The customer with given id was not found');

    res.send(customer);
  }
  catch(ex){
    res.status(400).send(ex.message);
  }
})

router.delete('/:id',async (req,res) => {
  try{
    const customer = await Customer.findByIdAndRemove(req.params.id);
    
    if(!customer) return res.status(404).send('The customer with the given id was not found');

    res.send(customer);
  }
  catch(ex){
    res.status(400).send(ex.message);
  }
})

module.exports = router;