const {auth} = require('../middleware/auth');
const { User, validateUser : validate} = require('../models/user');
const express = require('express');
const router = express.Router();
const _ = require('lodash');
const bcrypt = require('bcrypt');

/*router.get('/',async (req,res) => {
  const user = await User.find().select('name email -_id');
  res.send(user);
})*/

router.get('/me',auth,async (req,res)=>{
  const user = await User.findById(req.user._id).select('-password');
  res.send(user);
})

router.post('/',async (req,res) => {
  //const { title, genre } = req.body // {title: 'something', genre:'something'}

   const { error } = validate(req.body);
   if(error) return res.status(400).send(error.details[0].message);

   let user = await User.findOne({email : req.body.email});
   if(user) return res.status(400).send('User is already registered.');

   user = new User(_.pick(req.body,['name','email','password']));

   const salt = await bcrypt.genSalt(10);
   user.password = await bcrypt.hash(user.password,salt);

   /*user = new User({
     name : req.body.name,
     email : req.body.email,
     password : req.body.password
   })*/

   await user.save();

   const token = user.generateAuthToken();

   //jwt.sign({_id : user._id},config.get('jwtPrivateKey'));

   res.header('x-auth-token',token).send(_.pick(user,['name','email']));
   /*
   res.send(
    {
      name : user.name,
      email : user.email
    });*/
   
})

module.exports = router;