const { Rental,validateRental:validate } = require('../models/rental');
const { Movie } = require('../models/movie');
const { Customer } = require('../models/customer');
const express = require('express');
const router = express.Router();


router.get('/',async (req,res) => {
  const rental = await Rental.find();
  res.send(rental);
})

router.post('/',async (req,res) => {
  //const { title, genre } = req.body // {title: 'something', genre:'something'}

   const { error } = validate(req.body);
   if(error) return res.status(400).send(error.details[0].message);

   const customer = await Customer.findById(req.body.customerId);
   if(!customer) return res.status(400).send('Invalid Customer');

   const movie = await Movie.findById(req.body.movieId);
   if(!movie) return res.status(400).send('Invalid Movie');

   if (movie.numberInStock === 0) return res.status(400).send('Movie is out of stock');

  let rental = new Rental({
    Customer : {
      _id : customer.id,
      name : customer.name,
      phone : customer.phone
    },
    Movie : {
      _id : movie.id,
      title : movie.title,
      dailyRentalRate : movie.dailyRentalRate
    },
    rentalFee : req.body.rentalFee
  });

   rental = await rental.save();

   movie.numberInStock--;
   await movie.save();
  
   res.send(rental);
})

module.exports = router;