const {auth} = require('../middleware/auth');
const {admin} = require('../middleware/admin');
const {Genre,validateGenre : validate} = require('../models/genre');
const express = require('express');
const router = express.Router();

router.get('/:id',async (req,res) => {
    try{
      const genre = await Genre.findById(req.params.id);
 
      if(!genre) return res.status(404).send('The Genre with the given id was not found');
      
      res.send(genre);
    }
    catch(ex){
      res.status(400).send(ex.message);
    }
  })


router.get('/',async (req,res) => {
  try{
    const genre = await Genre.find().sort('name').select('-__v');
    res.send(genre);
  }
  catch(ex){
    res.status(500).send('Something failed.');
  }
});

router.post('/',auth,async (req,res) => {
const { error } = validate(req.body);
if(error) return res.status(400).send(error.details[0].message);
let genre =  new Genre({ name : req.body.name });
 genre = await genre.save();

 res.send(genre);
})


router.put('/:id',async (req,res) => {
  try{
    const { error } = validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    //let genre = await Genre.findByIdAndUpdate(req.params.id,{name : req.body.name},{new : true});

   let genre = await Genre.findById(req.params.id);

   if(!genre) return res.status(404).send('The genre with the given id was not found');

   genre.name = req.body.name;
   
   genre = await genre.save(); 

    res.send(genre);
  }
  catch(ex){
    console.log(ex);
    return res.status(400).send(ex.message);
  }

})

router.delete('/:id',[auth,admin],async (req,res) => {
  try{
    const genre = await Genre.findByIdAndRemove(req.params.id);
  
    if (!genre) return res.status(404).send('The Genre with the given id was not found');

    res.send(genre);
  }
  catch(ex){
    return res.status(400).send(ex.message);
  }

})

module.exports = router;